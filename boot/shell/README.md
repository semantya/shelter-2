Environment variables :
=======================

* SHL_ROOT      : The Shelter's root path.
* SHL_PATH      : The Shelter's system path for executables.

* SHL_OS        : Tells on which operating system the Shelter is running.
* SHL_OS_SPECS  : Tells where are the OS specs

* SHL_HOME_ROOT : Tells the root's home directory path.
* SHL_HOME_DIR  : Tells the users home directory path.

* SHL_CHARMS_DIR : Directory from which we loads IT charms.

Shell Aliases :
===============

# Shell listing enhancement

alias lh='ll -h'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias lsize='ls -l $1 | wc -l'

# Common shell helpers

alias random='od -vAn -N4 -tu4 < /dev/urandom'
alias ssh-clear='ssh-keygen -f $HOME/.ssh/known_hosts -R '

# Supervisor's shortcuts

alias superctl='supervisorctl'
alias superstart='supervisorctl start'
alias superrestart='supervisorctl restart'
alias superstop='supervisorctl stop'
alias supertail='supervisorctl tail -f'
