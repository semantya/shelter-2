# Shell listing enhancement

alias lh='ll -h'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias lsize='ls -l $1 | wc -l'

# Common shell helpers

alias eradicate='killall -s KILL -v'
alias random='od -vAn -N4 -tu4 < /dev/urandom'
alias ssh-clear='ssh-keygen -f $HOME/.ssh/known_hosts -R '

# Supervisor's shortcuts

alias superctl='supervisorctl'
alias superstart='supervisorctl start'
alias superrestart='supervisorctl restart'
alias superstop='supervisorctl stop'
alias supertail='supervisorctl tail -f'
