export GHOST_DIR="/ghost"

export HML_ROOT="/hml"
export SHL_ROOT="/shl"

export SHL_PATH=$SHL_ROOT'/sbin'

export SHL_HOME_ROOT=/root
export SHL_HOME_DIR=/home

#export SHL_CHARMS_DIR=$GHOST_DIR/ext
export SHL_CHARMS_DIR=$SHL_ROOT/usr/charms

################################################################################################################################

if [[ -f /etc/os-release ]] ; then
    source /etc/os-release

    for key in ID ID_LIKE NAME PRETTY_NAME VERSION VERSION_ID HOME_URL SUPPORT_URL BUG_REPORT_URL ANSI_COLOR ; do
        eval "export DISTRIB_"$key"=\$"$key

        unset $key
    done
fi

#*******************************************************************************************************************************

case $DISTRIB_ID in
    raspbian)
        export SHL_OS="raspbian"
        ;;
    Debian)
        export SHL_OS="debian"
        ;;
    ubuntu|Ubuntu)
        export SHL_OS="ubuntu"
        ;;
    *)
        case $OSTYPE in
            cygwin)
                export SHL_OS="windows"

                export SHL_HOME_ROOT=$HOME
                export SHL_HOME_DIR=$HOMEDRIVE"\Users"
                ;;
            *)
                export SHL_OS="linux"

                if [[ -f /usr/bin/yum ]] ; then
                    export SHL_OS="redhat"
                fi

                if [[ -d /Users ]] ; then
                    export SHL_OS="macosx"

                    export SHL_HOME_ROOT=/var/root
                    export SHL_HOME_DIR=/Users
                fi
                ;;
        esac
    ;;
esac

#*******************************************************************************************************************************

export SHL_OS_SPECS=$SHL_ROOT/boot/os/$SHL_OS

if [[ ! -d $SHL_OS_SPECS ]] ; then
    git clone https://bitbucket.org/os2use/$SHL_OS.git $SHL_OS_SPECS
fi

if [[ ! -d $SHL_OS_SPECS ]] ; then
    echo "Could'nt find the specs for your Operating System : "$SHL_OS

    echo "Sorry, the Shelter can't bootup. Exiting ..."

    exit
fi

source $SHL_OS_SPECS/env.sh

if [[ -d $SHL_OS_SPECS/tools ]] ; then
    SHL_PATH=$SHL_PATH":"$SHL_OS_SPECS/tools
fi

#source $SHL_ROOT/boot/shell/env.sh

################################################################################################################################

if [[ -d $HML_ROOT/jitsu ]] ; then
    for key in `ls $HML_ROOT/jitsu` ; do
        if [[ -e $HML_ROOT/jitsu/$key/env.sh ]] ; then
            source $HML_ROOT/jitsu/$key/env.sh
        fi

        if [[ -d $HML_ROOT/jitsu/$key/sbin ]] ; then
            SHL_PATH=$SHL_PATH":"$HML_ROOT/jitsu/$key/sbin
        fi
    done
fi

if [[ -d $HML_ROOT/sdk ]] ; then
    for key in `ls $HML_ROOT/sdk` ; do
        if [[ -e $HML_ROOT/sdk/$key/env.sh ]] ; then
            source $HML_ROOT/sdk/$key/env.sh
        fi

        if [[ -d $HML_ROOT/sdk/$key/sbin ]] ; then
            SHL_PATH=$SHL_PATH":"$HML_ROOT/sdk/$key/sbin
        fi
    done
fi

#*******************************************************************************************************************************

#for key in `echo $SHL_SDK` ; do
#    init_$key
#done

#*******************************************************************************************************************************

for LANG_NAME in `ls $SHL_ROOT/lib` ; do
    export LANG_INCLUDES=$SHL_ROOT/lib/$LANG_NAME/dist

    for nrw in `ls $SHL_CHARMS_DIR` ; do
        if [[ -d $SHL_CHARMS_DIR/$nrw/lib/$LANG_NAME ]] ; then
          LANG_INCLUDES=$LANG_INCLUDES" "$SHL_CHARMS_DIR/$nrw/lib/$LANG_NAME
        fi
    done

    for nrw in `ls $HML_ROOT/jitsu` ; do
        if [[ -d $HML_ROOT/jitsu/$nrw/lib/$LANG_NAME ]] ; then
          LANG_INCLUDES=$LANG_INCLUDES" "$HML_ROOT/jitsu/$nrw/lib/$LANG_NAME
        fi
    done

    for nrw in `ls $HML_ROOT/sdk` ; do
        if [[ -d $HML_ROOT/sdk/$nrw/lib/$LANG_NAME ]] ; then
          LANG_INCLUDES=$LANG_INCLUDES" "$HML_ROOT/sdk/$nrw/lib/$LANG_NAME
        fi
    done

    echo "LANG) $LANG_NAME :"
    echo "\t-> Includes : "$LANG_INCLUDES

    if [[ -e $SHL_ROOT/lib/$LANG_NAME/env.sh ]] ; then
        source $SHL_ROOT/lib/$LANG_NAME/env.sh
    fi

    if [[ -d $SHL_ROOT/lib/$LANG_NAME/tools ]] ; then
        SHL_PATH=$SHL_PATH":"$SHL_ROOT/lib/$LANG_NAME/tools
    fi

    unset LANG_INCLUDES
done

################################################################################################################################

if [[ -e $HML_ROOT/recipes ]] ; then
    for key in `ls $HML_ROOT/recipes` ; do
        if [[ -e $HML_ROOT/recipes/$key/env.sh ]] ; then
            source $HML_ROOT/recipes/$key/env.sh
        fi

        if [[ -d $HML_ROOT/recipes/$key/sbin ]] ; then
            SHL_PATH=$SHL_PATH":"$HML_ROOT/recipes/$key/sbin
        fi
    done
fi

#*******************************************************************************************************************************

for key in `ls $SHL_CHARMS_DIR` ; do
    if [[ -d $SHL_CHARMS_DIR/$key ]] ; then
        cd $SHL_CHARMS_DIR/$key

        if [[ -f $SHL_CHARMS_DIR/$key/env.sh ]] ; then
            source $SHL_CHARMS_DIR/$key/env.sh
        fi

        if [[ -d $SHL_CHARMS_DIR/$key/sbin ]] ; then
            SHL_PATH=$SHL_PATH":"$SHL_CHARMS_DIR/$key/sbin
        fi
    fi
done

#*******************************************************************************************************************************

if [ $SHL_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",nano"
    export CYG_PKGs=$CYG_PKGs",bash,dash,fish,mksh,zsh"
    export CYG_PKGs=$CYG_PKGs",tree,htop,nmap"
fi

if [ $SHL_OS=="macosx" ] ; then
    export BREW_PKGs=$BREW_PKGs" mksh"
fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" mksh zsh zsh-lovers"
    export APT_PKGs=$APT_PKGs" tree htop fping nmap traceroute"
fi

export NPM_PKGs="$NPM_PKGs nsh"

source $SHL_ROOT/boot/shell/aliases.sh

#*******************************************************************************************************************************

export SHL_PATH

source /etc/environment

if [[ -f $HOME/.shelter/env.sh ]] ; then
     source $HOME/.shelter/env.sh
fi
