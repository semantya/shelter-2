#!/bin/zsh

plugins=( ant cloudapp compleat command-not-found coffee django docker encode64 gem gitfast git-extras git-flow git-hubflow github grails jira heroku history history-substring-search jira knife knife_ssh last-working-dir lol mvn nanoc node npm nyan phing pip postgres profiles pyenv python rails rake rand-quote redis-cli ruby rvm screen supervisor torrent urltools vagrant virtualenv virtualenvwrapper web-search wd)

source /shl/boot/shell/env.sh

export PATH=$SHL_PATH":"$PATH


if [[ -f $HOME/.shelter/profile.zsh ]] ; then
     source $HOME/.shelter/profile.zsh
else
     ZSH_THEME="xiong-chiamiov-plus"
     plugins=(ant cloudapp compleat command-not-found coffee django docker encode64 gem gpg-agent gitfast git-extras git-flow git-hubflow github grails jira heroku history history-substring-search jira knife knife_ssh last-working-dir lol mvn nanoc node npm nyan phing pip postgres profiles pyenv python rails rake rand-quote redis-cli ruby rvm screen supervisor torrent urltools vagrant virtualenv virtualenvwrapper web-search wd)
fi

ZSH=$SHL_ROOT/boot/shell/zsh/oh-my-zsh

export UPDATE_ZSH_DAYS=13

CASE_SENSITIVE="false"
DISABLE_AUTO_UPDATE="true"
COMPLETION_WAITING_DOTS="true"

DEBIAN_PREVENT_KEYBOARD_CHANGES=yes
# DISABLE_UNTRACKED_FILES_DIRTY="true"

export DEBIAN_PREVENT_KEYBOARD_CHANGES=yes




#function printc () {
#    print "printc" $1
#}

#autoload -Uz  add-zsh-hook

#add-zsh-hook preexec printc





source $ZSH/oh-my-zsh.sh
