Preparing the system :
===========================

Depending on your operating system, follow the steps defined in the links below :

* [RedHat](https://bitbucket.org/os2use/redhat)

* [Debian](https://bitbucket.org/os2use/debian)
* [Ubuntu 14.04+](https://bitbucket.org/os2use/ubuntu)

For embedded systems, you can check :

* [Raspbian](https://bitbucket.org/os2use/raspbian)
* [Snappy (Ubuntu Core)](https://bitbucket.org/os2use/snappy)

For compliancy-testing purposes, you can check :

* [Mac OS X](https://bitbucket.org/os2use/macosx)
* [Windows 7+](https://bitbucket.org/os2use/windows)

Installing the shelter :
========================

Run the following commands :

```shell
git clone git@bitbucket.org:it2use/shelter.git  /shl

cd /shl

git submodule update --init --recursive

if [[ ! -d /hml ]] ; then
    git clone git@bitbucket.org:it2use/homeless.git /hml

    cd /hml

    git submodule update --init --recursive

    git remote rm origin
fi
```

Then, using BASH :

```bash
source boot/shell/bash/env.sh

/shl/boot/strap
```

OR, for the ZSH lovers :

```zsh
source boot/shell/zsh/env.sh

/shl/boot/strap
```

Setting up your machine :
=========================

First, setup your machine by following the wizard after executing  :

* shelter init

Becoming homeless :
==================

Proceed by setting up your [Homeless](https://bitbucket.org/it4you/homeless) specs, then fall down the rabbit hole :

* homeless help

Discovering the neighboorhood :
===============================

```shell
shelter help
```

Enjoy hacking your potential !-)
