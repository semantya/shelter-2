#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CHOCO_PKGs=$CHOCO_PKGs" ruby1.9 rubygems"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	export BREW_PKGs=$BREW_PKGs" nodejs"
#fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" ruby1.9.3 rubygems"
fi

export RUBY_PKGs="fpm god foreman bluepill"
export RUBY_PKGs="$RUBY_PKGs dydra"
export RUBY_PKGs="$RUBY_PKGs rhc heroku af"
export RUBY_PKGs="$RUBY_PKGs cloudapp_api"

alias ruby-install="gem install --no-rdoc --no-ri"
