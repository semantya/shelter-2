#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CHOCO_PKGs=$CHOCO_PKGs" python"
    export CYG_PKGs=$CYG_PKGs",python-argparse,python-avahi,python-lxml"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	export BREW_PKGs=$BREW_PKGs" nodejs"
#fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs="$APT_PKGs `echo python-{setuptools,software-properties,virtualenv}`"
fi

export PYPI_PKGs="httpie"
export PYPI_PKGs="$PYPI_PKGs gdata googlecl"

export PYTHONPATH=""
#export PYTHONPATH=`python -c "import sys ; print ':'.join([x for x in sys.path if x])"`

for pth in `echo $LANG_INCLUDES` ; do
    export PYTHONPATH="$PYTHONPATH:$pth"
done

#export SHL_PATH=$SHL_PATH":"$SHL_ROOT"/venv/bin"
#source $SHL_ROOT/venv/bin/activate

alias python-install="pip install -U"

alias vpython="venv/bin/python"
alias vpip="venv/bin/pip"
alias vscrapy="venv/bin/scrapy"

alias dj_manage="python manage.py"
alias dj_syncdb="python manage.py syncdb --noinput"
alias dj_evolve="python manage.py evolve --hint --execute --noinput"
alias dj_server="python manage.py runserver"
alias dj_debug="python manage.py runserver_plus"

alias djv_manage="vpython manage.py"
alias djv_syncdb="vpython manage.py syncdb --noinput"
alias djv_evolve="vpython manage.py evolve --hint --execute --noinput"
alias djv_server="vpython manage.py runserver"
alias djv_debug="vpython manage.py runserver_plus"

alias django2use="vpython django2use/manage.py"
