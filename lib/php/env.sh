#!/bin/sh

export PHP_PATH=$SHL_ROOT"/lib/php/dist:"$SHL_ROOT"/lib/php/dist"

for pth in $LANG_INCLUDES ; do
  export PHP_PATH=$PHP_PATH":"$pth
done

if [ $SHL_OS=="windows" ] ; then
    export CYG_PKGs="$CYG_PKGs,bash,dash,fish,mksh,zsh"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	export BREW_PKGs=$BREW_PKGs" nodejs"
#fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs="$APT_PKGs php-pear `echo php5-{gd,imap,memcache,mysql,sqlite}`"
fi

export PHP_PKGs="mongo"

alias php-install="pecl install"
